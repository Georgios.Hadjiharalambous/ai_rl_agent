"""
  University of Glasgow
  Artificial Intelligence 2019-2020
  Assessed Exercise

  Basic demo code for the CUSTOM Open AI Gym problem used in AI (H/M) '19-'20

  Version: 20192020a

  Tested on Python 3.7 (Anaconda)

"""

# Imports

import gym
import numpy as np
import time
from uofgsocsai import LochLomondEnv # load the class defining the custom Open AI Gym problem
import os, sys
from helpers import *
import time
from collections import defaultdict
from aimautils import *
import random
#We make random agent as a function to be easier to call from evaluation script.
def rl_agent(problem_id=0,reward_hole=0.0,is_stochastic=True,max_episodes=10000,max_iter_per_episode=2000,map_name_base = "8x8-base"):
    seconds = time.time()
    # Generate the specific problem
    env = LochLomondEnv(problem_id=problem_id, is_stochastic=is_stochastic, map_name_base=map_name_base, reward_hole=reward_hole)
    # Let's visualize the problem/env
    # Reset the random generator to a known state (for reproducibility)
    np.random.seed(12)
    #these are used if you want to choose as exploration function the frequency of states visited (taken from the lab)
    Ne=50  # iteration limit in exploration function
    Rplus=2  # large value to assign before iteration limit
    alpha=lambda n: 60./(59+n)  #decorator

    gamma=0.9#Q-learning discount factor

    def f(u, n):#function taken from AI LAB, used in exploration by frequency of each state.
            """ Exploration function. Returns fixed Rplus until
            agent has visited state, action a Ne number of times.
            Same as ADP agent in book."""
            if n < Ne:
                return Rplus
            else:
                return u
    def choose_action_freq(state):#checks the we did this action in this state a lot of times
    #i.e visited the next state a lot of times, to see if we did enought exploration.
    #If we explored enought of all the actions from this state, the Q value is returned.
        action=0
        b=np.array([f(Q[state, a1], Nsa[state, a1]) for a1 in range(4)])
        action=np.random.choice(np.flatnonzero(b == b.max()))
        return action

        #different exploration function. It uses probabilities to find whether it will explore or exploit Q table
        #We use a decreasing over time exploration_rate.
    def choose_action(state):
        action=0
        if np.random.uniform(0, 1) < exploration_rate:
            action = env.action_space.sample()
        else:
            action = np.argmax(Q[state, :])#choose the action that corresponds to the largest Q-value
        return action

        #function to update the Q values depended on the next state.
    def learn(state, state2, reward, action):
        predict = Q[state, action]
        target = reward + gamma * np.max(Q[state2, :])
        Q[state, action] = Q[state, action] + learning_rate * (target - predict)

    Q=np.zeros([env.observation_space.n, env.action_space.n])#Q table
    Nsa=np.zeros([env.observation_space.n, env.action_space.n])#table to keep the frequency of each action on each state
    learning_rate = 0.4
    exploration_rate =.5

    number_of_solutions_for_100_episodes=0
    number_of_successes_per_100_episode=[]

    number_of_failures_for_100_episodes=0
    number_of_failures_per_100_episodes=[]
    number_of_min_steps_for_solution=max_iter_per_episode
    number_of_max_steps_for_solution=0
    reward_on_100_episodes_sum=0
    rewards_per_100_episodes=[]

    solutions_found_per_steps=[]
    solutions_found_per_phase=[]

    for e in range(max_episodes): # iterate over episodes
        state = env.reset() # reset the state of the env to the starting state
        for iter in range(max_iter_per_episode):
          #action = choose_action_freq(state)#uncomment this and comment the next, if you want to try this exploration function
          #it gives similar results. Although this function has more logic behind its decisions rather than the other, we use
          #the other for ease.
          action = choose_action(state)#idea to use function for action choosing and learning taken from
          # url : https://medium.com/swlh/introduction-to-reinforcement-learning-coding-q-learning-part-3-9778366a41c0
          Nsa[state,action]+=1#increase frequency of visiting this state and action.
          state2, reward, done, info = env.step(action)
          learn(state, state2, reward, action)
          state = state2

          if(done and reward==reward_hole):
              number_of_failures_for_100_episodes+=1
              break

          if (done and reward == +1.0):
             if(number_of_min_steps_for_solution>iter):
                 number_of_min_steps_for_solution=iter
             if(number_of_max_steps_for_solution<iter):
                number_of_max_steps_for_solution=iter
             number_of_solutions_for_100_episodes+=1
             solutions_found_per_steps.append(iter+1)
             break
        reward_on_100_episodes_sum+=reward
        if(e%100==0):#collect different measurements per 100 episodes for statistics.
            number_of_successes_per_100_episode.append(number_of_solutions_for_100_episodes)
            number_of_failures_per_100_episodes.append(number_of_failures_for_100_episodes)
            rewards_per_100_episodes.append(reward_on_100_episodes_sum/100)
            reward_on_100_episodes_sum=0
            number_of_solutions_for_100_episodes=0
            number_of_failures_for_100_episodes=0
            exploration_rate*=.9#every 100 episodes reduce the exploration rate as we learn the Q values.

        if(e==10000):#end of training phase, starting evalutation
            learning_rate=0#no more learning
            exploration_rate=0#no more exploration, only use of Q values. Should change accordinly for other exploration function
            solutions_found_per_phase.append(solutions_found_per_steps)
            solutions_found_per_steps=[]


    solutions_found_per_phase.append(solutions_found_per_steps)
    number_of_max_steps_for_solution+=1
    number_of_min_steps_for_solution+=1
    stats={
            'rewards_per_100_episodes':rewards_per_100_episodes,
            'number_of_successes_per_100_episode':number_of_successes_per_100_episode,
            'solutions_found_per_phase':solutions_found_per_phase,
            'number_of_failures_per_100_episodes':number_of_failures_per_100_episodes,
            'number_of_max_steps_for_solution':number_of_max_steps_for_solution,
            'number_of_min_steps_for_solution':number_of_min_steps_for_solution
            }
    print('RL -> maximun steps for solution:',number_of_max_steps_for_solution)
    print('RL -> minimun steps for solution:',number_of_min_steps_for_solution)
    return stats

def main():
    if len(sys.argv) < 3:#check if the correct arguments are given.
        print("Please call me with at least 2 parameter")
        print('example usage run_rl.py 6 8x8-base')
        sys.exit()
    problem_id=int(sys.argv[1])
    map_name_base=sys.argv[2]
    rl_agent(reward_hole=-0.01,max_episodes=20000,is_stochastic=True,map_name_base = map_name_base,problem_id=problem_id,max_iter_per_episode=800)
if __name__=="__main__":
    main()
