"""
  University of Glasgow
  Artificial Intelligence 2019-2020
  Assessed Exercise

  Basic demo code for the CUSTOM Open AI Gym problem used in AI (H/M) '19-'20

  Version: 20192020a

  Tested on Python 3.7 (Anaconda)

"""

# Imports
import gym
import numpy as np
import time
from uofgsocsai import LochLomondEnv # load the class defining the custom Open AI Gym problem
import os, sys
from helpers import *
import time #to measure training time if needed.
#We make random agent as a function to be easier to call from evaluation script.
def random_agent(problem_id=0,reward_hole=0.0,is_stochastic=True,max_episodes=40000,max_iter_per_episode=2000,map_name_base = "8x8-base"):
    seconds = time.time()
    # Generate the specific problem
    env = LochLomondEnv(problem_id=problem_id, is_stochastic=is_stochastic, map_name_base=map_name_base, reward_hole=reward_hole)
    print(env.desc)#Just to see the map and where all objects are
    np.random.seed(12)

    #initialization of variables to keep stats for the experiment.
    number_of_solutions_for_100_episodes=0#number of times it reaches a solution for 100 episodes
    number_of_successes_per_100_episode=[] #number of times it reached a solution per 100 episodes for all episodes
    #same as above but for failures, i.e it fell into a hole and stopped.
    number_of_failures_for_100_episodes=0
    number_of_failures_per_100_episodes=[]
    number_of_min_steps_for_solution=max_iter_per_episode
    number_of_max_steps_for_solution=0

    reward_on_100_episodes_sum=0#rewards over 100 episodes
    rewards_per_100_episodes=[]#rewards over all episodes

    solutions_found_per_steps=[]

    for e in range(max_episodes): # iterate over episodes
        observation = env.reset() # reset the state of the env to the starting state

        for iter in range(max_iter_per_episode):

          action = env.action_space.sample() # your agent goes here (the current agent takes random actions)
          observation, reward, done, info = env.step(action) # observe what happends when you take the action

          if(done and reward==reward_hole):#agent fell into a hole we stop
              number_of_failures_for_100_episodes+=1
              break

          if (done and reward == +1.0):#agent reached the goal.We stop and get the reward
            number_of_solutions_for_100_episodes+=1
            if(number_of_min_steps_for_solution>iter):
                number_of_min_steps_for_solution=iter
            if(number_of_max_steps_for_solution<iter):
                number_of_max_steps_for_solution=iter
            solutions_found_per_steps.append(iter+1)
            break
        reward_on_100_episodes_sum+=reward
        if(e%100==0):#gather the statistics every 100 episodes
            number_of_successes_per_100_episode.append(number_of_solutions_for_100_episodes)
            number_of_failures_per_100_episodes.append(number_of_failures_for_100_episodes)
            rewards_per_100_episodes.append(reward_on_100_episodes_sum/100)
            reward_on_100_episodes_sum=0
            number_of_solutions_for_100_episodes=0
            number_of_failures_for_100_episodes=0
    number_of_max_steps_for_solution+=1
    number_of_min_steps_for_solution+=1
    stats={
            'rewards_per_100_episodes':rewards_per_100_episodes,
            'number_of_successes_per_100_episode':number_of_successes_per_100_episode,
            'solutions_found_per_steps':solutions_found_per_steps,
            'number_of_failures_per_100_episodes':number_of_failures_per_100_episodes,
            'number_of_max_steps_for_solution':number_of_max_steps_for_solution,
            'number_of_min_steps_for_solution':number_of_min_steps_for_solution
            }
    print('random agent -> maximun steps for solution:',number_of_max_steps_for_solution)
    print('random agent -> minimun steps for solution:',number_of_min_steps_for_solution)
    return stats

def main():
    if len(sys.argv) < 3:#check if the correct arguments are given.
        print("Please call me with atleast 2 parameter")
        print('example usage run_random.py 6 8x8-base')
        sys.exit()
    problem_id=int(sys.argv[1])
    map_name_base=sys.argv[2]
    stats_rand=random_agent(problem_id=problem_id,map_name_base=map_name_base)
if __name__ == '__main__':
    main()
