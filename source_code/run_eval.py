from run_random import random_agent
from run_simple import simple_agent
from run_rl import rl_agent
import numpy as np
import matplotlib.pyplot as plt
import sys
import os

#function to  easily run all problems and agents on a specific maps size
#this functions trains/evaluates the agents performance and makes figures about their performances and in general produces
#statistics.
def run_and_evaluate(maps=["8x8-base"]):
    map_size=int(maps[0].split("x")[0])#get the map numbers to produce
    problems_id=np.arange(0,map_size)#the problems to run
    stats_rl={}
    stats_random_agent={}
    stats_simple={}
    reward_hole=-.01#the reward for the hole, could be zero.
    max_iters_per_map={4:1000,8:2000,16:8000}
    max_iter_per_episode=max_iters_per_map[map_size]
    max_episodes=20000
    #here we run and evaluate our three agents on all problems.
    #The results are saved in a dictionary for each agent.
    for problem_id in problems_id:
        for map in maps:
            if(map=='4x4-base'):
                if(problem_id<4):
                    stats_rl[map,problem_id]=rl_agent(problem_id=problem_id,reward_hole=reward_hole,is_stochastic=True,max_episodes=max_episodes,max_iter_per_episode=max_iter_per_episode,map_name_base =map)
                    stats_random_agent[map,problem_id]=random_agent(max_episodes=max_episodes,map_name_base = map,problem_id=problem_id,is_stochastic=True,max_iter_per_episode=max_iter_per_episode)
                    stats_simple[map,problem_id]=simple_agent(map_name_base = map,problem_id=problem_id)
            else:
                stats_rl[map,problem_id]=rl_agent(problem_id=problem_id,reward_hole=reward_hole,is_stochastic=True,max_episodes=max_episodes,max_iter_per_episode=max_iter_per_episode,map_name_base =map)
                stats_random_agent[map,problem_id]=random_agent(max_episodes=max_episodes,map_name_base = map,problem_id=problem_id,is_stochastic=True,max_iter_per_episode=max_iter_per_episode)
                stats_simple[map,problem_id]=simple_agent(map_name_base = map,problem_id=problem_id)

    print('------------------------------')
    print('Stats for RL agent           -')
    print('------------------------------')
    rl_rewards_all_problems=[] #rewards for all episodes of RL agent to make an average graph of performance
    for stats in stats_rl:
        stat=stats_rl[stats]
        _,id=stats#find the problem id
        #gather all the statistics from the dictionary
        rewards_per_100_episodes=stat['rewards_per_100_episodes']
        rl_rewards_all_problems.append(rewards_per_100_episodes)
        number_of_successes_per_100_episode=stat['number_of_successes_per_100_episode']
        number_of_failures_per_100_episodes=stat['number_of_failures_per_100_episodes']
        solutions_found_per_phase=stat['solutions_found_per_phase']
        ##we have to check if empty
        if(len(solutions_found_per_phase[0])>0):
            solutions_found_per_steps_training=np.mean(solutions_found_per_phase[0])#mean number
        else:
            solutions_found_per_steps_training=0
        if(len(solutions_found_per_phase[1])>0):
            solutions_found_per_steps_evaluation=np.mean(solutions_found_per_phase[1])#mean number
        else:
            solutions_found_per_steps_evaluation=0
        print('solutions_found_per_steps_training on problem id=%d'%id,solutions_found_per_steps_training)
        print('solutions_found_per_steps_evaluation  on problem id=%d'%id,solutions_found_per_steps_evaluation)
        print('RL reached the goal %d times on training:'%len(solutions_found_per_phase[0]),'id=',id)
        print('RL reached the goal %d times on evaluation:'%len(solutions_found_per_phase[1]),'id=',id)
        #creating the figure for successes and failures
        x=np.arange(len(number_of_successes_per_100_episode))*100#xaxis of figure
        fig = plt.gcf()
        plt.title('number of successes and failures per 100 episodes on problem id ='+str(id))
        plt.plot(x,number_of_successes_per_100_episode,label='successes')
        plt.plot(x,number_of_failures_per_100_episodes,label='failures')
        plt.xlabel('number of episodes')
        plt.legend()
        base_path='./'+map#create a folder to store the figures.
        path='/rl'
        if not os.path.exists(base_path):
            os.makedirs(base_path)
        if not os.path.exists(base_path+path):
            os.makedirs(base_path+path)
        fig.savefig(base_path+path+'/id='+str(id)+' number of successes and failures per 100 episodes rew='+str(reward_hole)+'.png')
        plt.close()
        #create figure on rewards per episode
        x=np.arange(len(rewards_per_100_episodes))*100
        fig = plt.gcf()
        plt.title('rewards per 100 episodes on problem id ='+str(id))
        plt.xlabel('number of episodes')
        plt.ylabel("reward")
        plt.plot(x,rewards_per_100_episodes)
        plt.savefig(base_path+path+'/id='+str(id)+'_rewards_per_100_episodes_rew='+str(reward_hole)+'.png')
        plt.close()

    print('------------------------------')
    print('Stats for random agent       -')
    print('------------------------------')

    random_rewards_all_problems=[]#rewards for all episodes of random agent to make an average graph of performance
    for stats in stats_random_agent:
        stat=stats_random_agent[stats]
        _,id=stats#getting the problem id
        #gathering some statistics about the agent
        rewards_per_100_episodes=stat['rewards_per_100_episodes']
        random_rewards_all_problems.append(rewards_per_100_episodes)
        number_of_successes_per_100_episode=stat['number_of_successes_per_100_episode']
        number_of_failures_per_100_episodes=stat['number_of_failures_per_100_episodes']
        solutions_found_per_steps=stat['solutions_found_per_steps']
        if(len(solutions_found_per_steps)>0):
            solutions_found_per_steps_training=np.mean(solutions_found_per_steps)#mean number
        else:
            solutions_found_per_steps_training=0
        print('solutions_found_per_steps_training on problem id=%d'%id,solutions_found_per_steps_training)
        print('random reached the goal %d times:'%len(solutions_found_per_steps),'id=',id)
        #creating the figure for successes and failures
        x=np.arange(len(number_of_successes_per_100_episode))*100
        fig = plt.gcf()
        plt.title('number of successes and failures per 100 episodes on problem id ='+str(id))
        plt.xlabel('number of episodes')
        plt.plot(x,number_of_successes_per_100_episode,label='successes')
        plt.plot(x,number_of_failures_per_100_episodes,label='failures')
        plt.legend()
        path='/random'
        base_path='./'+map
        if not os.path.exists(base_path):
            os.makedirs(base_path)
        if not os.path.exists(base_path+path):
            os.makedirs(base_path+path)
        fig.savefig(base_path+path+'/id='+str(id)+' number of successes and failures per 100 episodes rew='+str(reward_hole)+'.png')
        plt.close()
        #create figure on rewards per episode
        x=np.arange(len(rewards_per_100_episodes))*100
        fig = plt.gcf()
        plt.title('rewards per 100 episodes on problem id ='+str(id))
        plt.xlabel('number of episodes')
        plt.ylabel("reward")
        plt.plot(x,rewards_per_100_episodes)
        plt.savefig(base_path+path+'/id='+str(id)+'_rewards_per_100_episodes_rew='+str(reward_hole)+'.png')
        plt.close()

    print('------------------------------')
    print('Stats for A* simple agent    -')
    print('------------------------------')
    for stats in stats_simple:
        stat=stats_simple[stats]
        _,id=stats#the only crusial stat simple agent has is the steps needed for each problem.
        print('solutions found per steps on problem id =%d' %id,stat['solutions_found_per_steps'] )
    #Create the figure for the performance of all agents against each other averaged on all problems.
    x=np.arange(len(random_rewards_all_problems[0]))*100
    random=np.zeros(len(random_rewards_all_problems[0]))
    rl=np.zeros(len(rl_rewards_all_problems[0]))
    #Summing the rewards of each agent across episodes
    for i in range (len(random_rewards_all_problems)):
        random+=random_rewards_all_problems[i]
        rl+=rl_rewards_all_problems[i]
    random/=len(random_rewards_all_problems)
    rl/=len(random_rewards_all_problems)
    #plotting
    plt.title('training and evalutation of all agents \n averaged on all problems of %s maps'%map)
    plt.plot(x,random,label='random agent')
    plt.plot(x,rl,label='RL agent')
    plt.plot(x,[1]*len(x),label='A* agent')
    plt.xlabel('number of episodes')
    plt.ylabel('averaged reward over 100 episodes')
    y=np.linspace(0,1,num=10)
    plt.plot([10000]*len(y),y,'--',label='training stop episode')#a line to mark that traning stops in this episode.
    if (map=='4x4-base'):
        plt.legend(loc='center right')
    else :
        plt.legend(loc='upper left')

    plt.savefig(base_path+'/overal rewards_per_100_episodes hole='+str(reward_hole)+'.png')



def main():
    if len(sys.argv) < 2:
        print("Please call me with 1 parameter")
        print('example usage run_eval.py 8x8-base')
        map_name_base=["8x8-base"]#,"16x16-base","4x4-base"]
    else:
        map_name_base=[sys.argv[1]]
    run_and_evaluate(map_name_base)
if __name__ == '__main__':
    main()
