Created by Georgios Hadjiharalambous during the AI course @ University of Glasgow.
Student id:2486083h

In order to run you must install openai through python.
For example:
$pip install gym 

To run the evaluation script write in a terminal:
$python run_eval.py 8x8-base

8x8-base, is the basic map used. 
16x16-base and 4x4-base could be used instead.

To run the agents individually write in a terminal:

$python run_rl.py 6 8x8-base

$python run_random.py 6 8x8-base

$python run_simple.py 6 8x8-base

The first argument is the problem id, which could be [0-3] for 4x4 map,
[0-7] for 8x8 map and [0-15] for 16x16 map.

If in unix, we provide a cleanup script to remove the folders for each map's evaluration and the cached python files.
$./clean_folders.sh
