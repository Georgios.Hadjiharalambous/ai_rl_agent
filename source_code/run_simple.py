"""
  University of Glasgow
  Artificial Intelligence 2019-2020
  Assessed Exercise

  Basic demo code for the CUSTOM Open AI Gym problem used in AI (H/M) '19-'20

  Version: 20192020a

  Tested on Python 3.7 (Anaconda)

"""

# Imports
import gym
import numpy as np
import time
from uofgsocsai import LochLomondEnv # load the class defining the custom Open AI Gym problem
import os, sys
from helpers import *
import time
from search import *
#We make random agent as a function to be easier to call from evaluation script.
def simple_agent(problem_id=0,reward_hole=0.0,is_stochastic=False,max_episodes=200000,max_iter_per_episode=2000,map_name_base = "8x8-base"):
    # Generate the specific problem
    env = LochLomondEnv(problem_id=problem_id, is_stochastic=is_stochastic, map_name_base=map_name_base, reward_hole=reward_hole)
    # Create a representation of the state space for use with AIMA A-star
    state_space_locations, state_space_actions, state_initial_id, state_goal_id = env2statespace(env)
    # Reset the random generator to a known state (for reproducibility)
    np.random.seed(12)

    #the code below was taken from the AI LAB and AIMA toolbox.
    #It provides implementation of graph traversal algorithm and along with A* implementation and appropriate heuristics
    #used we can find an optimal minimal path towards the goal.
    #################################################################################################################
    def my_best_first_graph_search_for_vis(problem, f):
        """Search the nodes with the lowest f scores first.
        You specify the function f(node) that you want to minimize; for example,
        if f is a heuristic estimate to the goal, then we have greedy best
        first search; if f is node.depth then we have breadth-first search.
        There is a subtlety: the line "f = memoize(f, 'f')" means that the f
        values will be cached on the nodes as they are computed. So after doing
        a best first search you can examine the f values of the path returned."""

        # we use these two variables at the time of visualisations
        iterations = 0
        all_node_colors = []
        node_colors = {k : 'white' for k in problem.graph.nodes()}

        f = memoize(f, 'f')
        node = Node(problem.initial)

        node_colors[node.state] = "red"
        iterations += 1
        all_node_colors.append(dict(node_colors))

        if problem.goal_test(node.state):
            node_colors[node.state] = "green"
            iterations += 1
            all_node_colors.append(dict(node_colors))
            return(iterations, all_node_colors, node)

        frontier = PriorityQueue('min', f)
        frontier.append(node)

        node_colors[node.state] = "orange"
        iterations += 1
        all_node_colors.append(dict(node_colors))

        explored = set()
        while frontier:
            node = frontier.pop()

            node_colors[node.state] = "red"
            iterations += 1
            all_node_colors.append(dict(node_colors))

            if problem.goal_test(node.state):
                node_colors[node.state] = "green"
                iterations += 1
                all_node_colors.append(dict(node_colors))
                return(iterations, all_node_colors, node)

            explored.add(node.state)
            for child in node.expand(problem):
                if child.state not in explored and child not in frontier:
                    frontier.append(child)
                    node_colors[child.state] = "orange"
                    iterations += 1
                    all_node_colors.append(dict(node_colors))
                elif child in frontier:
                    incumbent = frontier[child]
                    if f(child) < f(incumbent):
                        del frontier[incumbent]
                        frontier.append(child)
                        node_colors[child.state] = "orange"
                        iterations += 1
                        all_node_colors.append(dict(node_colors))

            node_colors[node.state] = "gray"
            iterations += 1
            all_node_colors.append(dict(node_colors))
        return None

    def my_astar_search_graph(problem, h=None):
        """A* search is best-first graph search with f(n) = g(n)+h(n).
        You need to specify the h function when you call astar_search, or
        else in your Problem subclass."""
        h = memoize(h )#or problem.h, 'h')
        iterations, all_node_colors, node = my_best_first_graph_search_for_vis(problem,
                                                                    lambda n: n.path_cost + h(problem.goal,n))
        return(iterations, all_node_colors, node)
    #################################################################################################################
    #END of lab code.

    #heuristics function to be used in A*.
    #Its called Manhatan distance and is admisible. So can guarantee along the use of A* an optimal solution.
    def heuristic(nodeBegin,nodeEnd):
        nodeBegin,nodeEnd=str(nodeBegin).split('_'),str(nodeEnd).split('_')
        nodeBegin_end=nodeBegin[2].split('>')[0]
        nodeEnd_end=nodeEnd[2].split('>')[0]
        xB,yB=int(nodeBegin[1]),int(nodeBegin_end)
        xE,yE=int(nodeEnd[1]),int(nodeEnd_end)
        dist=abs(xB-xE)+abs(yB-yE)
        return dist

    # Initiliaze map-graph so A* can run and find the solution
    loch_lomond_map= UndirectedGraph(state_space_actions)
    loch_lomond_map.locations=state_space_locations
    loch_lomond_problem = GraphProblem(state_initial_id, state_goal_id , loch_lomond_map)

    all_node_colors=[]
    iterations, all_node_colors, node = my_astar_search_graph(problem=loch_lomond_problem, h=heuristic)

    #-- Trace the solution --#
    solution_path = [node]
    cnode = node.parent
    solution_path.append(cnode)
    while cnode.state != state_initial_id:
        cnode = cnode.parent
        solution_path.append(cnode)

    actions = {3: [-1,0],1: [+1,0],2: [0,+1], 0: [0,-1]}
    #function to find the action corresponding to that transition in order to find the actions taken to reach the goal.
    def find_action(nodeBegin,nodeEnd):
            nodeBegin,nodeEnd=str(nodeBegin).split('_'),str(nodeEnd).split('_')
            nodeBegin_end=nodeBegin[2].split('>')[0]
            nodeEnd_end=nodeEnd[2].split('>')[0]
            xB,yB=int(nodeBegin[1]),int(nodeBegin_end)
            b=[xB,yB]
            xE,yE=int(nodeEnd[1]),int(nodeEnd_end)
            e=[xE,yE]
            for act in actions:
                a=actions[act]
                x=[e[0]+int(a[0]),e[1]+int(a[1])]
                if x==b:
                    return act
    #find the path towards the solution.
    path=[]
    for i in range(len(solution_path)-1):
        path.append(find_action(solution_path[i],solution_path[i+1]))
    path=path[::-1]
    num=int(np.sqrt(env.observation_space.n))
    grid=np.ones((num,num))#create a grid to represent the path on the screen
    solutions_found_per_steps=0
    observation = env.reset()
    #Use the path found the check if it leads towards a solution
    for i in range(0,len(path)):
            #env.render()#uncomment if you want to see the steps.
            x=observation%num#find the grid coordinates corresponding to this state
            y=observation//num
            grid[y,x]=555#specify the grid state that is a state from the path using 555 as a symbol.
            action=path[i]
            observation, reward, done, info = env.step(action)
            if done:
                solutions_found_per_steps=(i+1)
                x=observation%num
                y=observation//num
                grid[y,x]=555
                #env.render()#uncomment if you want to see the steps to reach the goal.
                break
    print('path to goal, 555 is used as to create the path, problem id = ',problem_id)
    print()            
    print(grid)#uncomment if you want to see the solution path
    stats={'solutions_found_per_steps':solutions_found_per_steps}
    return stats
def main():
    if len(sys.argv) < 3:
        print("Please call me with atleast 2 parameter")
        print('example usage run_simple.py 6 8x8-base')
        sys.exit()
    problem_id=int(sys.argv[1])
    map_name_base=sys.argv[2]
    stats_simple=simple_agent(problem_id=problem_id,map_name_base = map_name_base)
if __name__ == '__main__':
    main()
